﻿using System;
using System.Collections.Generic;

namespace laboratorio5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Conta> contas = new List<Conta>();
            ContaPoupanca c1 = new ContaPoupanca(0.02M, new DateTime(2020,9,15), "Júlio");
            contas.Add(c1);
            contas.Add(new ContaPoupanca(0.02M, new DateTime(2020,9,1), "Ana"));
            c1.Depositar(100);
            c1.AdicionarRendimento();
            foreach (var item in contas)
            {
                Console.WriteLine($"{item.Id}: {item.Saldo:C}");
            }
        }
    }
}
