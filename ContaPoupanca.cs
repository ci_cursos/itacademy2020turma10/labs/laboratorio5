using System;

namespace laboratorio5
{
    public class ContaPoupanca : Conta
    {
        private decimal taxaJuros;
        private DateTime dataAniversario;
        public ContaPoupanca(decimal j, DateTime d, string t) : base(t)
        {
            taxaJuros = j;
            dataAniversario = d;
        }
        public decimal Juros
        {
            get {return taxaJuros;}
            set {taxaJuros = value;}
        }
        public DateTime DataAniversario
        {
            get {return dataAniversario;}
        }
        public override string Id
        {
            get {return $"{Titular}(CP)";}
        }
        public void AdicionarRendimento()
        {
            DateTime hoje = DateTime.Now;
            if (hoje.Day == dataAniversario.Day && hoje.Month == dataAniversario.Month)
            {
                decimal rendimento = Saldo * taxaJuros;
                Depositar(rendimento);
            }
        }
    }
}